package roadgraph;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is the super class of the classes can represent node.
 * @author luning
 *
 */
public class Node {
	protected List<Edge> edges;
	private int index;
	
	/**
	 * 
	 * @param i index of this node
	 */
	public Node(int i) {
		edges = new ArrayList<>();
		index = i;
	}
	
	public int getIndex() {
		return index;
	}
	
	/**
	 * Get all neighbors of this node
	 * @return  all neighbors
	 */
	
	public <S extends Node> List<S> neighbors(){
		List<S> neighbors =  new ArrayList<>();
		for (Edge e : edges) {
			if(e.start().getIndex()==this.index ){ // starts at this node then adds end node
				neighbors.add(e.end());
			}else if (e.end().index == this.index) { // ends at this node then adds start node
				neighbors.add(e.start());
			}
		}
		return neighbors;
		
	}
	/**add edge in out fashion
	 * 
	 * @param e edge to be added into adjacent list
	 */
	public void addOutEdge(Edge e)
	{
		this.edges.add(e);
	}

}
