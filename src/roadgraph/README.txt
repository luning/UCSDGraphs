Class: MapGraph

Modifications made to MapGraph (what and why):

1. private HashMap<GeographicPoint, Intersection> nodes
inner data sturcture for indexing and storing nodes.

2. private int numNodes = 0;
inner data stucture for tracing nodes

3. private int numEdges = 0;
inner data structure for tracing edges

4. public void printGraph()
for debugging

5. public void printPath(List<GeographicPoint> path)
for debugging


Class name: Node

Purpose and description of class:
Since this is an assignment of designing class for a graph application, I think the class design should not be 
only adapted to the roadmap, but also other applications. The graph data strurcture is composed of node and edge.
No matter what applications this is, node and edge have some common properties and behivours. Then I create node and edge 
super class, so that this can be adpated to new application easily, say airline graph.

Class name: Edge

Purpose and description of class:

See above

Class name: Intersection

Purpose and description of class:

In the road map appliction, the node is an intersection which constains a location. 


Class name: Road

Purpose and description of class:

In the road map appliction, the edge is an road which has name, type and length. 

Overall Design Justification (4-6 sentences):

In this assignment, I design a class hiearchy for Map application(Class_hierarchy.jpg in the folder is the UML). My design includes basic grpah representation and behaviors. I make two abstractions that the vertex and edge are designed as Node and Edge superclass which consists of common properties and 
behaviours in the Graph ADT. Technically, the concept of design can be view as a kind of decoupling and generalization which make 
refactor and extentison more flexible.
