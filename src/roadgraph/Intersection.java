/**
 * 
 */
package roadgraph;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import geography.GeographicPoint;

/**
 * @author luning
 *
 */
public class Intersection extends Node {

	private GeographicPoint location;
	private HashMap<GeographicPoint, Double> distanceMap;
	private HashMap<GeographicPoint, Double> timeCostMap;
	
	/**
	 * @param i index of this intersection
	 */
	public Intersection(int i, GeographicPoint location) {
		super(i);
		this.location = location;
	}
	
	public GeographicPoint getLoacation() {
		return location;
	}
	
	public double getDistanceTo(GeographicPoint to) {
		
		if (distanceMap==null) {
			distanceMap = new HashMap<>();
			for (Edge e : edges) {					
					distanceMap.put( ((Intersection)e.end()).getLoacation(), ((Road)e).getLength() );		
			}
		}
		if (!distanceMap.containsKey(to)) {
			return 0.0;
		}else {
			return distanceMap.get(to);
		}
	}
	
	
	public double getTravelTimeTo(GeographicPoint to) {
		
		if (timeCostMap==null) {
			timeCostMap = new HashMap<>();
			for (Edge e : edges) {					
				timeCostMap.put( ((Intersection)e.end()).getLoacation(), ((Road)e).getTimeCost() );		
			}
		}
		if (!timeCostMap.containsKey(to)) {
			return Double.POSITIVE_INFINITY;
		}else {
			return timeCostMap.get(to);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Intersection> neighbors(){
		List<Intersection> neighbors =  new ArrayList<>();
		for (Edge e : edges) {
			if(e.start().getIndex()==this.getIndex() ){ // starts at this node then adds end node
				neighbors.add(e.end());
			}else if (e.end().getIndex() == this.getIndex()) { // ends at this node then adds start node
				neighbors.add(e.start());
			}
		}
		return neighbors;
		
	}
	

}
