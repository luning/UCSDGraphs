package roadgraph;

public class Edge {
	
	private Node from;
	private Node to;

	public Edge(Node from, Node to) {
		this.from = from;
		this.to = to;
	}
	
	@SuppressWarnings("unchecked")
	public <S extends Node> S start() {
		return (S)this.from;
	}
	
	@SuppressWarnings("unchecked")
	public <S extends Node> S end() {
		return (S)this.to;
	}

}
