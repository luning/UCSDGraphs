/**
 * 
 */
package roadgraph;


/**
 * @author luning
 *
 */
public class Road extends Edge {

	/**
	 * 
	 */
	
	
	
	private static enum SPEEDLIMIT { 
		UNCLASSIFIED(25,"UNCLASSIFIED"),RESIDENTIAL(40,"RESIDENTIAL"), TERTIARY(60,"TERTIARY"),
		TERTIARY_LINK(60,"TERTIARY_LINK"),SECONDARY(80,"SECONDARY"), SECONDARY_LINK(80,"SECONDARY_LINK"),
	PRIMARY(100,"PRIMARY"),PRIMARY_LINK(100,"PRIMARY_LINK"),MOTORWAY_LINK(120,"MOTORWAY_LINK"), MOTORWAY(120,"MOTORWAY");
		private final int speedLimit;
		private final String typename;
		SPEEDLIMIT(int p,String name){
			this.speedLimit = p;
			this.typename = name;
		}
		
		public int getLimit() {
			return this.speedLimit;
		}
		
		@Override
		public String toString(){
			return this.typename;
		}
	};
	
	private String roadName;
	private String roadType;
	private double length;
	
	public Road(Intersection from, Intersection to) {
		super((Node)from, (Node)to);
	}
	
	public Road(String roadName, String roadType, double length, Intersection from, Intersection to){
		super((Node)from, (Node)to);
		this.roadName = roadName;
		this.roadType = roadType;
		this.length = length;
	}
	
	
	public double getTimeCost() {
		int limit = 0;
		
		switch (this.roadType.toUpperCase()) {
		
		case "UNCLASSIFIED" :
			limit = SPEEDLIMIT.UNCLASSIFIED.getLimit();
			break;
		case "RESIDENTIAL" :
			limit = SPEEDLIMIT.RESIDENTIAL.getLimit();		
			break;
		case "TERTIARY" :
			limit = SPEEDLIMIT.TERTIARY.getLimit();	
			break;
		case "TERTIARY_LINK" :
			limit = SPEEDLIMIT.TERTIARY_LINK.getLimit();	
			break;
		case "SECONDARY" :
			limit = SPEEDLIMIT.SECONDARY.getLimit();	
			break;
		case "SECONDARY_LINK" :
			limit = SPEEDLIMIT.SECONDARY_LINK.getLimit();	
			break;
		case "PRIMARY" :
			limit = SPEEDLIMIT.PRIMARY.getLimit();	
			break;
		case "PRIMARY_LINK" :
			limit = SPEEDLIMIT.PRIMARY_LINK.getLimit();	
			break;
		case "MOTORWAY" :
			limit = SPEEDLIMIT.MOTORWAY.getLimit();	
			break;
		case "MOTORWAY_LINK" :
			limit = SPEEDLIMIT.MOTORWAY_LINK.getLimit();	
			break;

		default:
			limit = 25;
			break;
		}
		
		return length/limit;
		
	}
	
	public String getRoadName() {
		return roadName;
	}
	
	public void setRoadName(String roadName) {
		this.roadName = roadName;
	}
	
	public String getRoadType() {
		return roadType;
	}
	
	public void setRoadType(String roadType) {
		this.roadType = roadType;
	}
	
	public double getLength() {
		return length;
	}
	
	public void setLength(double length) {
		this.length = length;
	}
	

	

}
