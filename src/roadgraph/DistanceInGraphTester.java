package roadgraph;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import org.junit.Before;
import org.junit.Test;


import geography.GeographicPoint;
import util.GraphLoader;

public class DistanceInGraphTester {
	
	private PriorityQueue<DistanceInGraph> aDistanceInGraphs;
	private List<GeographicPoint> path;
	@Before
	public void setUp(){
		aDistanceInGraphs = new PriorityQueue<>(new DistanceInGraphComparator());
		GeographicPoint a1 = new GeographicPoint(1, 2);
		GeographicPoint a2 = new GeographicPoint(2, 3);
		GeographicPoint a3 = new GeographicPoint(3, 4);
		aDistanceInGraphs.add(new DistanceInGraph(a1));
		aDistanceInGraphs.add(new DistanceInGraph(a2));
		aDistanceInGraphs.add(new DistanceInGraph(a3));
		
		
		GeographicPoint b1 = new GeographicPoint(7.0, 3.0);
		GeographicPoint b2 = new GeographicPoint(4.0, 1.0);
		GeographicPoint b3 = new GeographicPoint(4.0, 0.0);
		GeographicPoint b4 = new GeographicPoint(4.0, -1.0);
		path = new ArrayList<>();
		
		path.add(b1);path.add(b2);path.add(b3);path.add(b4);
		
	}
	
	@Test
	public  void dijkstraTest() {
		MapGraph theMap = new MapGraph();
		GraphLoader.loadRoadMap("data/testdata/simpletest.map", theMap);
		//theMap.printGraph();
		GeographicPoint start = new GeographicPoint(7.0, 3.0);
		GeographicPoint goal = new GeographicPoint(4.0, -1.0);
		List<GeographicPoint> actual = theMap.dijkstra(start, goal);
		assertArrayEquals("dddd",path.toArray(), actual.toArray());
	}
	
	@Test
	public void getTest()
	{
		assertEquals(Double.POSITIVE_INFINITY,aDistanceInGraphs.peek().getDistance(),0.00000000001);
	}
	
	
	@Test
	public void ComparatorTest(){
		GeographicPoint a4 = new GeographicPoint(5, 6);
		DistanceInGraph distanceInGraph = new DistanceInGraph(a4,55);
		aDistanceInGraphs.add(distanceInGraph);

		assertEquals(55, aDistanceInGraphs.peek().getDistance(), 0.00000000001);
		
		
		GeographicPoint a5 = new GeographicPoint(7, 8);
		DistanceInGraph distanceInGraph2 = new DistanceInGraph(a5,12);
		aDistanceInGraphs.add(distanceInGraph2);

		assertEquals(12, aDistanceInGraphs.peek().getDistance(), 0.00000000001);
		
	}

}
